#include<iostream>

using namespace std;

int main(){

    int r1,r2,c1,c2;

    cout<<"Enter number of rows for matrix A\n";
    cin>>r1;

    cout<<"Enter number of columns for matrix A\n";
    cin>>c1;

    int a[r1][c1];

    for(int i=0;i<r1;i++){

        for(int j=0;j<c1;j++){

            cout<<"Enter value for A["<<i<<"]["<<j<<"]\n";
            cin>>a[i][j];
        }
    }


    cout<<"Enter number of rows for matrix B\n";
    cin>>r2;

    cout<<"Enter number of columns for matrix B\n";
    cin>>c2;

    int b[r2][c2];

    for(int i=0;i<r2;i++){

        for(int j=0;j<c2;j++){

            cout<<"Enter value for B["<<i<<"]["<<j<<"]\n";
            cin>>b[i][j];
        }
    }

    if(c1==r2){


        int d[r1][c2];

        for(int i=0;i<r1;i++){
            for(int j=0;j<c2;j++){
                int x=0;
                for(int k=0;k<c1;k++){
                    x=x+a[i][k]*b[k][j];
                }
                d[i][j]=x;
            }
        }

        for(int i=0;i<r1;i++){

            for(int j=0;j<c2;j++){

                cout<<d[i][j]<<" ";
            }
            cout<<"\n";
        }

    }else{
        cout<<"Matrix dimensions do not allow multiplication\n";
    }

    return 0;
}
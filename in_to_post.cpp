#include<iostream>
#include<ctype.h>
#define SIZE 20
using namespace std;

void push(char);
void pop();
int precedence(char);

int top=-1,pf=0;
char stack[SIZE];
char postfix[SIZE];
char input[] = {'A','+','(','B','*','C','-',
                '(','D','/','E','^','F',')','*','G',')','*','H'};


int main(){

    int size = sizeof(input)/sizeof(char);

    top++;
    stack[top]='(';
    int k=0;
    for(int i=0;i<size;i++){
        char scan=input[i];

        if(isalnum(scan)){
            postfix[pf]=scan;
            pf++;
        }else{

            if((precedence(scan)>precedence(stack[top])||scan=='(')&&scan!=')'){
                push(scan);
            }else if(scan==')'){
                while(stack[top]!='('){
                    pop();
                }
                //Removing open parentheses
                pop();
                
            }else{
                //Check relevance for checking if top is "(" not necessary though... better remove
                while(precedence(scan)<=precedence(stack[top])&&stack[top]!='('){
                    pop();

                }
                
                
                if(scan!=')'){
                    push(scan);
                }
                
            }
        }
    }

    while(top!=-1){
        pop();
    }

    int j=0;
    while(postfix[j]){
        cout<<postfix[j];
        j++;
    }

    return 0;
}

void push(char item){
    
    if(top+1<SIZE){
        top++;
        stack[top]=item;
    }else{
        cout<<"Overflow\n";
    }
    
}

void pop(){
    if(top!=-1){

        char item = stack[top];
        if(item!='('){
            postfix[pf]=item;
            pf++;
        }
        
        top--;
    }
    
}

int precedence(char scan){
    if(scan=='^'){
        return 3;
    }else if(scan=='*'||scan=='/'){
        return 2;
    }else if(scan=='+'||scan=='-'){
        return 1;
    }else{
        return 0;
    }
}
#include<iostream>
#include<malloc.h>

using namespace std;

struct tree{
    int info;
    struct tree *left;
    struct tree *right;
};

//Functional prototypes
struct tree * insert(struct tree *, int);
void search(struct tree *,int);
void inorder(struct tree *);
void postorder(struct tree *);
void preorder(struct tree *);

struct tree *root=NULL;

int main(){
    
    int ch,x;


    
    do{
        cout<<"\n1. Insert value";
        cout<<"\n2. Inorder";
        cout<<"\n3. Preorder";
        cout<<"\n4. Postorder";
        cout<<"\n5. Search value";
        cout<<"\n6. Exit\n";
        cin>>ch;

        switch(ch){
            case 1:
                cout<<"\nEnter value\n";
                cin>>x;
                root = insert(root,x);

                break;

            case 2:
                inorder(root);
                break;

            case 3:
                preorder(root);
                break;

            case 4:
                postorder(root);
                break;

            case 5:
                cout<<"\nEnter value to be searched\n";
                cin>>x;
                search(root,x);
                break;
            
            case 6:
                break;
            
            default:
                cout<<"\nWrong choice";
                break;
        }
    }while(ch!=6);

    return 0;
}

struct tree * insert(struct tree *node, int item){

    if(!node){
        node=new tree();
        node->info=item;
        node->left=NULL;
        node->right=NULL;

    }else if(item<node->info){
        node->left=insert(node->left,item);
    }else{
        node->right=insert(node->right,item);
    }
    return node;
    
}

void search(struct tree *root, int item){
    while(root){
        if(root->info==item){
            cout<<"\nElement found";
            break;
        }else if(item<root->info){
            root=root->left;
        }else{
            root=root->right;
        }
    }
    if(!root){
        cout<<"\nElement not found";
    }
}

void inorder(struct tree *node){
    if(node){
        inorder(node->left);
        cout<<" "<<node->info;
        inorder(node->right);
    }
}

void preorder(struct tree *node){
    if(node){
        cout<<" "<<node->info;
        preorder(node->left);
        preorder(node->right);
    }
}

void postorder(struct tree *node){
    if(node){
        postorder(node->left);
        postorder(node->right);
        cout<<" "<<node->info;
    }
}
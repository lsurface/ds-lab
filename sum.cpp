#include<iostream>

using namespace std;

//Finding sum and average

int main(){
    int n,sum=0;
    float avg;
    int a[50];

    cout<<"Enter array size:\n";
    cin>>n;

    //Creating new array
    for(int i=0;i<n;i++){
        cout<<"Enter value for a["<<i<<"]\n";
        cin>>a[i];
    }

    for(int i=0;i<n;i++){
        sum=sum+a[i];
    }

    avg=(float) sum/n;

    cout<<"Sum is "<<sum;
    cout<<"Average is "<<avg;
}
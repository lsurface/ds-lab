#include<iostream>

using namespace std;

//Finding mode

int main(){
    int n, freq=0,x;
    int a[50];

    cout<<"Enter array size:\n";
    cin>>n;

    //Creating new array
    for(int i=0;i<n;i++){
        cout<<"Enter value for a["<<i<<"]\n";
        cin>>a[i];
    }

    x=a[0];

    for(int i=0;i<n;i++){
        int count=0;
        for(int j=0;j<n;j++){
            if(a[i]==a[j]){
                count++;
            }
        }
        if(freq<count){
            freq=count;
            x=a[i];
        }
    }

    cout<<x<<" is the mode, with frequency "<<freq<<"\n";



    return 0;
}
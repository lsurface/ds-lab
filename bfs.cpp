#include<iostream>

using namespace std;

#define MAX_SIZE 20

int queue[MAX_SIZE];
bool graph[MAX_SIZE][MAX_SIZE];
int front=-1,rear=-1,n;

void enqueue(int);
int dequeue();
bool visited[MAX_SIZE];
void bfs(int);

int main(){
    
    cout<<"Enter number of nodes\n";
    cin>>n;

    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            cout<<"Enter value for node["<<i<<"]["<<j<<"]\n";
            cin>>graph[i][j];
        }
    }
    int source;
    cout<<"Enter source for BFS\n";
    cin>>source;

    bfs(source);
 

    return 0;
}

void enqueue(int item){
    if(rear=n-1){
        cout<<"Overflow";
    }else{
        if(front=-1){
            front=0;
        }
        rear++;
        queue[rear]=item;
    }
}

int dequeue(){
    int item;
    if(front==-1){
        cout<<"Underflow";
    }else{
        item=queue[front];
        rear--;
    }
}

void bfs(int sv){

    visited[sv]=1;

    for(int i=0;i<n;i++){
        if(graph[i][sv]){
            enqueue(i);
            cout<<"Pushed: "<<i<<"\n";
        }
    }
    while(top!=-1){
        int node=pop();
        cout<<"Popped: "<<node<<"\n";
        if(!visited[node]){
            for(int i=0;i<n;i++){
                if(graph[i][node]){
                    cout<<"Pushed: "<<i<<"\n";
                    push(i);
                }
            }
        visited[node]=1;
        }
        
    }

}
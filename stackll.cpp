#include<iostream>
#include<malloc.h>

using namespace std;

struct link{
    int info;
    struct link *next;
};

struct link *top;

void push(struct link *);
void pop(struct link *);
void display(struct link *);

int main(){

    //top=new link();
 

    int ch;
    struct link *node = new link();

    do{
        cout<<"\n1. Push";
        cout<<"\n2. Pop";
        cout<<"\n3. Display";
        cout<<"\n4. Terminate\n";
        cin>>ch;

        switch(ch){
            case 1:
                
                push(top);
                break;

            case 2:
                pop(top);
                break;
            case 3:
                display(top);
                break;
            case 4:
                break;
            default:
                cout<<"Wrong choice\n";
                break;
        }
    }while(ch!=4);

    
    

    return 0;

}

void push(struct link *node){
    struct link *temp = new link();
    cout<<"Enter node value\n";
    cin>>temp->info;

    if(top==NULL){
        top=temp;
        
    }else{
        
        temp->next=top;
        top=temp;
    }
    
}

void pop(struct link *node){
    
    if(top==NULL){
        cout<<"Underflow";
    }else{
        top=node->next;
        delete node;
    }
    

    
    
}

void display(struct link *node){
    /*
    do{
        cout<<node->info<<"\n";
        node=node->next;
    }while(node!=NULL);*/
    if(node==NULL){
        cout<<"Stack is empty";
    }else{
        while(node!=NULL){
            cout<<node->info<<"\n";
            node=node->next;
        }
    }
    
}

#include<iostream>

using namespace std;

int main(){

    int n,p;
    int a[50];

    cout<<"Enter array size:\n";
    cin>>n;

    //Creating new array
    for(int i=0;i<n;i++){
        cout<<"Enter value for a["<<i<<"]\n";
        cin>>a[i];
    }

    //Deleting value
    cout<<"Enter position of value to be deleted\n";
    cin>>p;

    p=p-1;

    for(int i=p;i<n-1;i++){
        a[i]=a[i+1];
    }

    n=n-1;

    for(int i=0;i<n;i++){
        cout<<a[i];
    }

    return 0;
    
}
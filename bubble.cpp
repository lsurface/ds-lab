#include<iostream>

using namespace std;

int main(){

    int a[20],n,temp;

    cout<<"Enter array size\n";
    cin>>n;

    cout<<"Enter array elements\n";
    for(int i=0;i<n;i++){
        cin>>a[i];
    }

    for(int i=0;i<n;i++){
        for(int j=0;j<n-1;j++){
            if(a[j]>a[j+1]){
                temp=a[j];
                a[j]=a[j+1];
                a[j+1]=temp;
            }
        }
    }

    cout<<"The sorted array is:\n";
    for(int i=0;i<n;i++){
        cout<<a[i]<<"\n";
    }

    return 0;
}
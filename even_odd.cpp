#include<iostream>

using namespace std;

int main(){

    int n,even=0,odd=0;
    int a[50],b[50],c[50];

    cout<<"Enter array size:\n";
    cin>>n;

    //Creating new array
    for(int i=0;i<n;i++){
        cout<<"Enter value for a["<<i<<"]\n";
        cin>>a[i];
    }

    for(int i=0;i<n;i++){

        //Even
        if(a[i]%2==0){
            b[even]=a[i];
            even++;
        }else{
            c[odd]=a[i];
            odd++;
        }
    }

    cout<<"Frequency of even numbers is "<<even;
    //View even numbers
    for(int i=0;i<even;i++){
        cout<<b[i];
    }

    cout<<"Frequency of odd numbers is "<<odd;
    //Displaying odd numbers
    for(int i=0;i<odd;i++){
        cout<<c[i];
    }



    return 0;
}
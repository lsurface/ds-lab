#include<iostream>
#include<malloc.h>

using namespace std;

void createlist(struct link *);
void insert(struct link *);
void deletion(struct link *);
void display(struct link *);
void revdisplay(struct link *);

struct link{
    int info;
    struct link *prev;
    struct link *next;
};

struct link *start=new link();


int main(){

    createlist(start);
    display(start);
    revdisplay(start);
    insert(start);
    display(start);
    deletion(start);
    display(start);
    return 0;
}

void createlist(struct link *node){
    int ch;
    struct link *node1;
    cout<<"Enter start node value\n";
    cin>>start->info;
    node=start;

    do{
        cout<<"Do you wish to continue? 1/0?";
        cin>>ch;

        switch(ch){
            case 0:
                break;

            case 1:
                node->next=new link();
                node1=node;
                node=node->next;
                node->prev=node1;
                cout<<"Enter node value\n";
                cin>>node->info;
                break;
        }
    }while(ch!=0);
}

void insert(struct link *node){

    struct link *curr=new link(),*node1;

    int loc,j=1,ch;

    cout<<"\nINSERTION OF A NEW NODE\n";
    cout<<"Enter node value\n";
    cin>>curr->info;

    cout<<"\n1. Insert at the beginning";
    cout<<"\n2. Insert at the end";
    cout<<"\n3. Insert in the middle";
    cin>>ch;

    switch(ch){
        case 1:
            curr->next=node;
            node->prev=curr;
            start=curr;
            break;

        case 2:
            while(node->next!=NULL){
                node=node->next;
            }

            node->next=curr;
            curr->prev=node;
            break;

        case 3:
            cout<<"Enter location\n";
            cin>>loc;

            while(j!=loc-1 && node!=NULL){
                node=node->next;
                j++;
            }

            if(j!=loc-1){
                cout<<"Location index exceeds link size\n";
            }else{
                node1=node->next;
                node1->prev=curr;
                curr->next=node->next;
                curr->prev=node;
                node->next=curr;
            }
            break;

        default:
            cout<<"Wrong choice\n";
            break;
    }
}

void deletion(struct link *node){

    struct link *node1;

    int ch,loc,j=1;
    cout<<"\nDELETION OF A NODE\n";
    cout<<"\n1. Delete at the beginning";
    cout<<"\n2. Delete at the end";
    cout<<"\n3. Delete in the middle";
    cin>>ch;

    switch(ch){
        case 1:
            start=node->next;
            start->prev=NULL;
            delete node;
            break;

        case 2:
            while(node->next!=NULL){
                node=node->next;
            }
            (node->prev)->next=NULL;
            delete node;
            break;

        case 3:
            cout<<"Enter location index";
            cin>>loc;

            while(j!=loc-1 && node!=NULL){
                node=node->next;
                j++;
            }

            if(j!=loc-1){
                cout<<"Location index is outside link size";
            }else{
                node1=node->prev;
                node1->next=node->next;
                (node->next)->prev=node1;
                delete node;
            }
            break;

    }
}

void display(struct link *node){
    while(node!=NULL){
        cout<<node->info<<"\n";
        node=node->next;
    }
}

void revdisplay(struct link *node){

    while(node->next!=NULL){
        node=node->next;
    }

    while(node!=NULL){
        cout<<node->info<<"\n";
        node=node->prev;
    }
}
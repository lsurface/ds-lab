#include<iostream>

using namespace std;

#define MAX_SIZE 20

int stack[MAX_SIZE];
bool graph[MAX_SIZE][MAX_SIZE];
int top=-1,n;

void push(int);
int pop();
bool visited[MAX_SIZE];
void dfs(int);

int main(){
    
    cout<<"Enter number of nodes\n";
    cin>>n;

    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            cout<<"Enter value for node["<<i<<"]["<<j<<"]\n";
            cin>>graph[i][j];
        }
    }
    int source;
    cout<<"Enter source for DFS\n";
    cin>>source;

    dfs(source);
    bool connected=true;
    int k=0;
    for(int i=0;i<n;i++){
        if(!visited[i]){
            cout<<i<<" "<<visited[i];
            connected=!connected;
            break;
        }
    }
    if(connected){
        cout<<"Graph is connected\n";
    }else{
        cout<<"Graph is not connected\n";
    }

    return 0;
}

void push(int val){
    top++;
    stack[top]=val;
}

int pop(){
    int item;
    if(top==-1){
        cout<<"Underflow";
    }else{
        item=stack[top];
        top--;
    }
    return item;
}

void dfs(int sv){

    visited[sv]=1;

    for(int i=0;i<n;i++){
        if(graph[i][sv]){
            push(i);
            cout<<"Pushed: "<<i<<"\n";
        }
    }
    while(top!=-1){
        int node=pop();
        cout<<"Popped: "<<node<<"\n";
        if(!visited[node]){
            for(int i=0;i<n;i++){
                if(graph[i][node]){
                    cout<<"Pushed: "<<i<<"\n";
                    push(i);
                }
            }
        visited[node]=1;
        }
        
    }

}
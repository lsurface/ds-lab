#include<iostream>

using namespace std;

//Searching for a value x

int main(){
    int n,x;
    int a[50];
    bool search=false;

    cout<<"Enter array size:\n";
    cin>>n;

    //Creating new array
    for(int i=0;i<n;i++){
        cout<<"Enter value for a["<<i<<"]\n";
        cin>>a[i];
    }

    cout<<"Enter value for x";
    cin>>x;

    for(int i=0;i<n;i++){
        if(a[i]==x){
            cout<<x<<" is found at "<<i+1<<"th position";
            search=true;
        }
    }

    if(!search){
        cout<<x<<" is not found in the array";
    }

    return 0;
}
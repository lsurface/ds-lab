#include<iostream>
#define MAX_SIZE 25

using namespace std;

void DFS(int);
void BFS(int);
void enqueue(int);
int dequeue();
void push(bool);
int pop();
int stack[MAX_SIZE];
bool stackempty();
bool queueempty();
int tree[5][5];
int queue[5];
int top=-1,front=-1,rear=-1;
int main(){
/*
    cout<<"Enter number of nodes:\n";
    cin>>n;
*/


    for(int i=0;i<5;i++){
        for(int j=0;j<5;j++){
            bool val;
            cout<<"Enter 1/0 if "<<i+1<<" is connected to "<<j+1;
            cin>>val;
            tree[i][j]=val;

        }
    }

    DFS(1);
    BFS(1);

    return 0;
}

void DFS(int sv){
    bool visited[5];
    push(sv);
    visited[sv]=1;

    while(!stackempty()){
        bool u=pop();
        for(int i=0;i<5;i++){
            if(tree[u][i]&&!visited[i]){
                push(i);
                visited[i]=1;
            }
        }
    }
}

void push(bool val){
    if(top+1<MAX_SIZE){
        top++;
        stack[top]=val;
    }else{
        cout<<"Overflow";
    }
}

int pop(){
    if(top!=-1){
        bool val = stack[top];
        top--;
        return val;
    }else{
        cout<<"Underflow";
    }
}

bool stackempty(){
    if(top==-1){
        return true;
    }else{
        return false;
    }
}
/*
bool queueempty(){
    if(front==-1||rear==-1){
        return true;
    }else{
        return false;
    }
}

void enqueue(int val){
    if(front==-1){
        front=0;
        rear=0;
        queue[rear]=val;
    }else{
        rear++;
        queue[rear]=val;
    }
}

int dequeue(){
    if(rear!=1){
        int val=queue[rear];
        rear--;
        return val; 
    }
}

void BFS(int sv){
    bool visited[5];
    enqueue(sv);
    visited[sv]=1;

    while(!queueempty()){
        int u=dequeue();
        for(int i=0;i<=5;i++){
            if(tree[u][i]&&!visited[i]){
                push(i);
                visited[i]=1;
            }
        }
    }
}
*/
#include<iostream>

using namespace std;

#define MAX 10

int stack[MAX], item;

int ch,top=-1,count=0,status=0;

void push(int item){
    if(top==(MAX-1)){
        cout<<"\n Stack Overflow";
    }else{
        stack[++top]=item;
    }
}

void pop(){
    int ret;
    if(top==-1){
        cout<<"\n Stack Underflow";
    }else{
        ret=stack[top--];
        cout<<"\n"<<ret;
    }
}

void display(){
    int i;
    
    cout<<"\n The stack contents are:";
    if(top==-1){
        cout<<"\n Stack is empty";
    }else{
        while(top!=-1){
            pop();
        }
    }
}

int main(){
    do{
        cout<<"\n\n----MAIN MENU----\n";
        cout<<"\n1. PUSH(Insert) in the Stack";
        cout<<"\n2. POP(Delete) from the Stack";
        cout<<"\n3. To terminate";
        cout<<"\nEnter your choice: ";
        cin>>ch;
        switch(ch){
            case 1:
                cout<<"\nEnter an element to be pushed";
                cin>>item;
                push(item);
                break;

            case 2:
                cout<<"\n Popped element is: ";
                pop();
                break;

            case 3:
                break;

            default:
                cout<<"\nInvalid Entry";

        }
    }while(ch!=3);

    display();
}
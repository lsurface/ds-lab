#include<malloc.h>
#include<iostream>

using namespace std;

struct link{
    
    int info;
    struct link *next;
};

struct link *start;

void createlist(struct link *);
void display(struct link *);
void insert(struct link *);
void deletion(struct link *);

int main(){

    start = new link();
    createlist(start);
    display(start);
    insert(start);
    display(start);
    deletion(start);
    display(start);

    return 0;
}

void createlist(struct link *node){
    
    int ch;

    cout<<"\nEnter the value of the value of the first node\n";
    cin>>node->info;
    node->next=NULL;

    cout<<"\nDo you want to continue? 1/0\n";
    cin>>ch;

    while(ch!=0){
        node->next = new link();
        node=node->next;
        node->next=NULL;
        cout<<"\nEnter the value of the node\n";
        cin>>node->info;
        cout<<"\nDo you want to continue 1/0\n";
        cin>>ch;
    }
}

void display(struct link *node){
    cout<<"\nThe values are:";
    while(node!=NULL){
        cout<<"\n"<<node->info;
        node=node->next;
    }
}

void insert(struct link *node){

    struct link * curr;
    int loc,j,ch;

    curr= new link();
    cout<<"\nINSERTION OF A NEW NODE\n";
    cout<<"\nEnter the value of the node\n";
    cin>>curr->info;
    cout<<"\n1. Insert at the beginning";
    cout<<"\n2. Insert at the end";
    cout<<"\n3. Insert at middle of the linked list";
    cout<<"\n Enter choice\n";
    cin>>ch;

    switch (ch){
        case 1:
            curr->next=node;
            start=curr;
            break;
        
        case 2:
            while(node->next!=NULL){
                node=node->next;
            }
            node->next=curr;
            curr->next=NULL;
            break;

        case 3:
            cout<<"\nEnter the location\n";
            cin>>loc;
            j=1;
            while(j!=loc-1){
                node=node->next;
                j++;
            }
            curr->next=node->next;
            node->next=curr;
            break;

        default:
            cout<<"\nWrong choice";
            break;
    }

}

void deletion(struct link *node){
    struct link *prev;
    int ch, loc,j;

    cout<<"\nDELETION OF A NODE\n";
    cout<<"\n1. Delete at the beginning";
    cout<<"\n2. Delete at the end";
    cout<<"\n3. Delete from the middle of the linked list";
    cout<<"\nEnter choice\n";
    cin>>ch;

    switch(ch){
        case 1:
            start=node->next;
            delete node;
            break;

        case 2:
            while(node->next!=NULL){
                prev=node;
                node=node->next;
            }

            prev->next=NULL;
            delete node;
            break;

        case 3:
            cout<<"\nEnter the location\n";
            cin>>loc;

            j=1;
            while(j!=loc){
                prev=node;
                node=node->next;
                j++;
            }

            prev->next=node->next;
            delete node;
            break;

        default:
            cout<<"\nWrong choice";
            break;
    }
}
#include<iostream>

using namespace std;

//Finding minimum and maximum

int main(){
    int n;
    int a[50];
    int min,max;

    cout<<"Enter array size:\n";
    cin>>n;

    //Creating new array
    for(int i=0;i<n;i++){
        cout<<"Enter value for a["<<i<<"]\n";
        cin>>a[i];
    }
    min=a[0];
    max=a[0];

    for(int i=1;i<n;i++){

        if(min>a[i]){
            min=a[i];
        }

        if(max<a[i]){
            max=a[i];
        }
    }

    cout<<"Minimum value is "<<min<<"\n";
    cout<<"Maximum value is "<<max<<"\n";

    return 0;
}
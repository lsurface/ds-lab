#include<iostream>

using namespace std;

int main(){

    int r,c;

    cout<<"Enter number of rows for matrix\n";
    cin>>r;

    cout<<"Enter number of columns for matrix\n";
    cin>>c;

    int a[r][c];

    for(int i=0;i<r;i++){

        for(int j=0;j<c;j++){

            cout<<"Enter value for a["<<i<<"]["<<j<<"]\n";
            cin>>a[i][j];
        }
    }

    for(int i=0;i<r;i++){

        for(int j=0;j<c;j++){

            cout<<a[i][j]<<" ";
        }
        cout<<"\n";
    }

}
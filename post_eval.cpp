#include<iostream>
#include<ctype.h>
#include<string>
#define SIZE 10
using namespace std;

char input[SIZE];
int stack[SIZE];

void push(int);
int pop();
void post_eval(int, int, char);

int top=-1;

int main(){

    int k=-1;
    bool query=1;
    do{
        k++;
        cout<<"Enter character\n";
        cin>>input[k];
        cout<<"Continue? 1/0?\n";
        cin>>query;
    }while(query&&k<SIZE);

    int j=0;
    while(input[j]){
        char scan = input[j];
        if(isdigit(scan)){
            
            push(scan-'0');
            

        }else{
            int op2 = pop();
            int op1 = pop();

            post_eval(op1,op2,scan);
        }
        j++;
    }

    cout<<stack[top];


    return 0;
}

void push(int opd){
    top++;
    stack[top]=opd;
}

int pop(){
    int opd = stack[top];
    top--;
    return opd;
}

void post_eval(int a, int b, char op){
    int c;
    switch(op){
        case '+':
            c=a+b;
            push(c);
            break;

        case '-':
            c=a-b;
            push(c);
            break;

        case '*':
            c=a*b;
            push(c);
            break;

        case '/':
            c=a/b;
            push(c);
            break;

        default:
            cout<<"Operator not defined\n";
            break;
    }
}
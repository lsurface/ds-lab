#include<iostream>

using namespace std;

struct term{
    int coef;
    int pow;
    struct term *next;
};

struct term *poly(){

    struct term *pol=new term();
    struct term *next_pol=new term();
    int ch;

    cout<<"Enter power: ";
    cin>>pol->pow;
    cout<< "Enter Coefficient: ";
    cin>>pol->coef;
    
    cout<<"Continue? ";
    cin>>ch;
    pol->next=next_pol;

        
    while(ch!=0){
        cout<<"Enter power: ";
        cin>>next_pol->pow;
        cout<<"Enter Coefficient: ";
        cin>>next_pol->coef;
        next_pol->next=new term();
        cout<<"Continue? ";
        cin>>ch;
        next_pol=next_pol->next;
    }
    

    return pol;

}

struct term *poly_add(struct term *poly1, struct term *poly2){
    cout<<"Adding";
    struct term *pol_init=new term();
    struct term *poly3=new term();

    
    if(poly1->pow>poly2->pow){
        pol_init->pow=poly1->pow;
        pol_init->coef=poly1->coef;
        poly1=poly1->next;
    }else if(poly1->pow<poly2->pow){
        pol_init->pow=poly2->pow;
        pol_init->coef=poly2->coef;
        poly2=poly2->next;
    }else{
        pol_init->pow=poly1->pow;
        pol_init->coef=poly1->coef+poly2->coef;
        poly1=poly1->next;
        poly2=poly2->next;
    }
    pol_init->next=poly3;

    while(poly1->next&&poly2->next){
        if(poly1->pow>poly2->pow){
            poly3->pow=poly1->pow;
            poly3->coef=poly1->coef;
            poly1=poly1->next;
        }else if(poly1->pow<poly2->pow){
            poly3->pow=poly2->pow;
            poly3->coef=poly2->coef;
            poly2=poly2->next;
        }else{
            poly3->pow=poly1->pow;
            poly3->coef=poly1->coef+poly2->coef;
            poly1=poly1->next;
            poly2=poly2->next;
        }

        poly3->next=new term();
        poly3=poly3->next;
    }

    struct term *ptrr=new term();
    if(poly1->next){
        ptrr=poly1;
    }else if(poly2->next){
        ptrr=poly2;
    }

    while(ptrr->next){
        poly3->pow=ptrr->pow;
        poly3->coef=ptrr->coef;
        poly3->next=new term();
        poly3=poly3->next;
        ptrr=ptrr->next;
    }



    return pol_init;
}

void display(struct term *node){
    while(node->next){
        cout<<node->coef<<" x^"<<node->pow<<" + ";
        node=node->next;
    }
}

int main(){
    struct term *poly1,*poly2,*poly3;
    cout<<"Define 1st polynomial\n";
    poly1=poly();


    cout<<"Define 2nd polynomial\n";
    poly2=poly();


    poly3=poly_add(poly1,poly2);
    cout<<"Displaying\n";
    display(poly3);
  
    return 0;
}
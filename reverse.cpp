#include<iostream>

using namespace std;

//Displaying array in reverse order
int main(){

    int n;
    int a[50];

    cout<<"Enter array size:\n";
    cin>>n;

    //Creating new array
    for(int i=0;i<n;i++){
        cout<<"Enter value for a["<<i<<"]\n";
        cin>>a[i];
    }

    for(int i=n-1;i>=0;i--){
        cout<<a[i];
    }

    return 0;

}
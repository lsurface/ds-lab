#include<iostream>

using namespace std;

void TOH(int n, char A, char B, char C){

    if(n>=1){

        TOH(n-1,A,C,B);
        cout<<A<<" to "<<C<<"\n";
        TOH(n-1,B,A,C);
    }
}

int main(){

    int n;

    cout<<"Enter number of disks\n";
    cin>>n;

    TOH(n,'S', 'A', 'D');

    return 0;
}
#include<iostream>

using namespace std;

int front=-1,rear,qsize=10;
int queue[10];

bool IsFull(){

    if(front==0 && rear==qsize-1){
        return true;
    }else{
        return false;
    }
}

bool isEmpty(){
    
    if(front==-1){
        return true;
    }else{
        return false;
    }
}

void enqueue(int element){

    if(IsFull()){

        cout<<"Queue is full\n";

    }else{

        if(isEmpty()){
            front=0;
            rear=0;
            queue[rear]=element;
            

        }else{
            rear++;
            queue[rear]=element;
        }
        cout<<"Inserted value is "<<element<<"\n";

    }

}

void dequeue(){

    if(isEmpty()){

        cout<<"Queue is empty\n";

    }else{
        cout<<"Deleted value is "<<queue[0];
        rear--;
        for(int i=0;i<=rear;i++){
            queue[i]=queue[i+1];
        }

        if(rear==-1){
            front=-1;
            
        }

    }

}

void display(){

    if(front==-1){
        cout<<"Queue is empty\n";
    }else{

        for(int i=front;i<=rear;i++){
        cout<<queue[i]<<"\n";
        }

    }

}

int main(){



    int ch;

    do{

        cout<<"\n1. Enqueue";
        cout<<"\n2. Dequeue";
        cout<<"\n3. Display queue";
        cout<<"\n4. Terminate\n";
        
        cin>>ch;
        cout<<"\n";
        
        switch(ch){

            case 1:
                int element;
                cout<<"Enter element to be queued\n";
                cin>>element;
                enqueue(element);
                break;
            
            case 2:
                dequeue(); 
                break;

            case 3:
                display();
                break;

            case 4:
                break;

            default:
                cout<<"\nInvalid Entry";
                break;
        }

    }while(ch!=4);

    return 0;
}
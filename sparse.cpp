#include<iostream>

using namespace std;

int main(){

    int n;

    cout<<"Enter matrix dimension\n";
    cin>>n;

    int a[n][n];

    for(int i=0;i<n;i++){

        for(int j=0;j<n;j++){
            cout<<"Enter value for a["<<i<<"]["<<j<<"]\n";
            cin>>a[i][j];
        }
    }

    int count;

    for(int i=0;i<n;i++){

        for(int j=0;j<n;j++){
            if(a[i][j]!=0){
                count++;
            }
        }
    }
    for(int i=0;i<n;i++){

        for(int j=0;j<n;j++){
            
            cout<<a[i][j]<<" ";
        }
        cout<<"\n";
    }

    int new_mat[3][count];
    int col=0;
    for(int i=0;i<n;i++){
        
        for(int j=0;j<n;j++){
            if(a[i][j]!=0){
                
                new_mat[0][col]=i;
                new_mat[1][col]=j;
                new_mat[2][col]=a[i][j];
                col++;
            }
        }
    }    
    
    for(int i=0;i<count;i++){
        
        cout<<"Row: "<<new_mat[0][i]<<"\n";
        cout<<"Column: "<<new_mat[1][i]<<"\n";
        cout<<"Value: "<<new_mat[2][i]<<"\n\n\n";
    }

    return 0;
}